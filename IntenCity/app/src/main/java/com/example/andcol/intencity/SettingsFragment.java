package com.example.andcol.intencity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SettingsFragment extends PreferenceFragmentCompat
        implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    private OnFragmentInteractionListener mListener;

    private HeatManager heatManager;

    public boolean onPreferenceClick(Preference preference) {
        /*
        Strictly talking, "wipe_db" is not really a preference, but rather a button that
        triggers an action in heatManager. onPreference click thus implements this behaviour.
         */
        switch (preference.getKey()) {
            case "wipe_db": {
                heatManager.resetDatabase();
                return true;
            }
            default: return false;
        }
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        /*
        Used to push preference changes to heatManager, which stores select_display and cooldown as
        local variables.
         */
        switch (preference.getKey()) {
            case "select_display": {
                int type = Integer.parseInt(newValue.toString());
                heatManager.setDisplay(type);
                heatManager.updateScreen(); //of course, after the map has changed, the screen needs to be updated
                return true;
            }
            case "cooldown": {
                //convert string to a numerical value
                switch (newValue.toString()) {
                    case "none" : heatManager.setCooldownTime(0); return true;
                    case "10m" : heatManager.setCooldownTime(10*HeatManager.ONE_MINUTE_WORTH_OF_SECONDS); return true;
                    case "1h" : heatManager.setCooldownTime(HeatManager.ONE_HOUR_WORTH_OF_SECONDS); return true;
                    case "1d" : heatManager.setCooldownTime(HeatManager.ONE_DAY_WORTH_OF_SECONDS); return true;
                    default : return false;
                }
            }
            default: return false;
        }
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences); //Add preferences from the preferences.xml file

        /*
        SettingsFragment needs access to the heatManager, to push some preference changes and to request
        database resets.
         */
        heatManager = HeatManager.getManager();

        //set listeners (the fragment implements both listeners)
        Preference wipeDb = findPreference("wipe_db");
        wipeDb.setOnPreferenceClickListener(this);

        Preference displayPref = findPreference("select_display");
        displayPref.setOnPreferenceChangeListener(this);
        Preference cooldownPref = findPreference("cooldown");
        cooldownPref.setOnPreferenceChangeListener(this);
    }


    //AUTO_GENERATED CODE FOLLOWS

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {}

    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
