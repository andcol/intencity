package com.example.andcol.intencity;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {Heat.class}, version = 2)
public abstract class HeatDatabase extends RoomDatabase {

    private static HeatDatabase INSTANCE; //HeatDatabase implements a singleton

    public abstract HeatDao heatDao();  //Data access objects for heats

    public static HeatDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (HeatDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context, HeatDatabase.class, "heat_database")
                            .addMigrations(MIGRATION_1_2).build();
                }
            }
        }
        return INSTANCE;
    }

    //migration from weightless heats to weighted ones
    static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //Just add weight 1 to all the existing heats
            database.execSQL("ALTER TABLE heat ADD heat_weight INTEGER NOT NULL DEFAULT 1");
        }
    };
}
