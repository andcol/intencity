package com.example.andcol.intencity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Query;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.Objects;

/*
A cell represents the basic unit of area encoding in IntenCity.
A cell is defined by its coordinates (cellX and cellY) and corresponds to a rectangular area of dimension
DEG_LONG_PER_CELL x DEG_LAT_PER_CELL on the map. It is used by heatManager to keep track of current area and such.
 */
public class Cell {

    //refer to documentation (PGST) for more information on PGST
    private static final double PGST_SCALE = 2000.0; //determines how many cells make up one longitudinal degree.
    private static final double PGST_LAT_FACTOR = 1.3; //together with PGST_SCALE, determines how many cells make up one latitudinal degree.

    private static final int CELLS_LAT = (int)(90*PGST_SCALE*PGST_LAT_FACTOR);
    private static final int CELLS_LNG = (int)(180*PGST_SCALE);
    private static final double DEG_LAT_PER_CELL = 180.0/CELLS_LAT;
    private static final double DEG_LNG_PER_CELL = 360.0/CELLS_LNG;

    @ColumnInfo(name = "cell_x")
    protected int cellX;
    @ColumnInfo(name = "cell_y")
    protected int cellY;

    @Override
    public boolean equals(Object object) {
        if(object.getClass() == Cell.class) {
            Cell tmp = (Cell)object;
            //two cells are equal whenever they have the same coordinates
            return (cellX == tmp.getCellX() && cellY == tmp.getCellY());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(cellX,cellY);
    }   //required for equals to work

    public Cell() {
        //Required empty constructor
    }

    public Cell(LatLng location) {
        //A cell is easily built from a location by integer-dividing it by how many lat/lng degrees there are per cell
        cellX = (int) Math.floor((location.longitude) / DEG_LNG_PER_CELL);
        cellY = (int) Math.floor((location.latitude) / DEG_LAT_PER_CELL);
    }

    //Returns the bounds of the cell in the form of a PolygonOptions object, ready to be rendered on the map
    public PolygonOptions getBounds() {
        LatLng farRight = new LatLng((cellY+1)*DEG_LAT_PER_CELL, (cellX+1)*DEG_LNG_PER_CELL);
        LatLng nearRight = new LatLng(cellY*DEG_LAT_PER_CELL, (cellX+1)*DEG_LNG_PER_CELL);
        LatLng nearLeft = new LatLng(cellY*DEG_LAT_PER_CELL, cellX*DEG_LNG_PER_CELL);
        LatLng farLeft = new LatLng((cellY+1)*DEG_LAT_PER_CELL, cellX*DEG_LNG_PER_CELL);

        return new PolygonOptions().add(farRight,nearRight,nearLeft,farLeft);
    }

    //Compute the gps coordinates of the center of the cell
    public LatLng getCenter() {
        return new LatLng(cellY*DEG_LAT_PER_CELL + DEG_LAT_PER_CELL/2, cellX*DEG_LNG_PER_CELL + DEG_LNG_PER_CELL);
    }

    public int getCellX() {
        return cellX;
    }

    public void setCellX(int cellX) {
        this.cellX = cellX;
    }

    public int getCellY() {
        return cellY;
    }

    public void setCellY(int cellY) {
        this.cellY = cellY;
    }
}
