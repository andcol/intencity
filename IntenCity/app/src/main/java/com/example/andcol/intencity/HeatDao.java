package com.example.andcol.intencity;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface HeatDao {

    //Simply insert a single heat into the heat's table
    @Insert
    void insert(Heat heat);

    //Retrieves the highest timestamp among the heats for a single cell (used for cooldown)
    @Query("SELECT max(timestamp) as timestamp FROM heat WHERE cell_x = :cellX AND cell_y = :cellY AND heat_type = :type")
    long getTimeOfLastMeasurementAtCellCoordinates(int type, int cellX, int cellY);

    /*
    Retrieves an averaged heat with
        -cell coordinates as specified
        -type as specified
        -rssi equal to the average rssi among the measurements of type "type" for the specified cell
        -the most recent timestamp among such measurements
        -weight equal to the number of such measurements
     */
    @Query("SELECT 0 AS heat_id, cell_x, cell_Y, heat_type, max(timestamp) AS timestamp, avg(heat_rssi) AS heat_rssi, sum(heat_weight) AS heat_weight " +
            "FROM heat " +
            "WHERE cell_x = :cellX AND cell_y = :cellY AND heat_type = :type")
    Heat getAveragedHeatAtCellCoordinates(int type, int cellX, int cellY);

    /*
    Does the same thing that getAveragedHeatAtCellCoordinates, but for ALL the cells in the range (minX,minY)-(maxX-maxY).
    Returns a list of averaged heats.
     */
    @Query("SELECT 0 AS heat_id, cell_x, cell_Y, heat_type, max(timestamp) AS timestamp, avg(heat_rssi) AS heat_rssi, sum(heat_weight) AS heat_weight " +
            "FROM heat " +
            "WHERE cell_x >= :minX AND cell_x <= :maxX AND cell_y >= :minY AND cell_y <= :maxY AND heat_type = :type " +
            "GROUP BY cell_x, cell_Y")
    List<Heat> getAveragedHeatsInArea(int type, int minX, int maxX, int minY, int maxY);
}
