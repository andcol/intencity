package com.example.andcol.intencity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;

import com.google.android.gms.maps.model.LatLng;
import java.sql.Timestamp;

/*
A Heat represents the basic unit of localized measurement in IntenCity.
Simply put, a Heat is a Cell with extra information regarding measurements and measurement meta-data.
Note that a heat can represent a single measurement OR the average of multiple measurements for the same area.
Heats are persisted using Room Persistence Library, thus the annotations.
 */
@Entity(tableName = "heat")
public class Heat extends Cell{

    public static final int TYPE_WIFI = 9;
    public static final int TYPE_UMTS = 2;
    public static final int TYPE_LTE = 18;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "heat_id")
    private int id; //used for persistence

    @ColumnInfo(name = "heat_type")
    private int measurementType;    //the type of the measurement (WiFi, Umts, Lte)
    @ColumnInfo(name = "heat_rssi")
    private int measurementRSSI;    //the signal strength of the measurement on a scale from 0 to 255

    @ColumnInfo(name = "timestamp")
    private long timestamp;         //Unix timestamp in seconds of the measurement

    //Ancillary field, used when building averaged heats
    @ColumnInfo(name= "heat_weight")
    private int weight = 1;

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public Heat() {
        //Required empty constructor
    }

    public Heat(Cell cell, int type, int strength) {
        timestamp = System.currentTimeMillis()/1000;

        //a Heat is build from a Cell
        this.cellX = cell.getCellX();
        this.cellY = cell.getCellY();

        measurementType = type;
        measurementRSSI = strength;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(int measurementType) {
        this.measurementType = measurementType;
    }

    public int getMeasurementRSSI() {
        return measurementRSSI;
    }

    public void setMeasurementRSSI(int measurementRSSI) {
        this.measurementRSSI = measurementRSSI;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
