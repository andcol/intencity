package com.example.andcol.intencity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.CellInfo;
import android.telephony.CellInfoWcdma;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class HeatManager {

    private static HeatManager INSTANCE; //the heat manager is a singleton

    //time constants
    public final static long ONE_MINUTE_WORTH_OF_SECONDS = 60;
    public final static long ONE_HOUR_WORTH_OF_SECONDS = 3600;
    public final static long ONE_DAY_WORTH_OF_SECONDS = 86400;

    //RSSI-related constants
    private final static int MIN_RSSI = 0;
    private final static int HALF_RSSI = 127;
    private final static int EXCELLENT_RSSI = 250;
    private final static int MAX_RSSI = 255;
    private final static double RSSI_DOUBLING_FACTOR = 255.0/127.0;

    //WiFi-related constants
    private final static double WIFI_SCALE_FACTOR = 0.5;
    private final static int MAX_WIFI_CONSIDERED = 3;
    private final static String WIFI_CHANNEL = "wifi_notification_channel";

    //Drawing constants
    private final static int MIN_DRAWABLE_ZOOM = 11;
    private final static int STROKE_UNSELECTED = 0;
    private final static int STROKE_SELECTED = 8;

    //"context" variables
    private GoogleMap map;
    private AppCompatActivity activity;

    //managers
    private WifiManager wifiManager;
    private TelephonyManager telephonyManager;

    private HeatDao heatDatabase;   //access to the heat's database

    //these keep track of the latest measurements for all three technologies
    private int latestWifiRSSI = -1;
    private int latestUmtsRSSI = -1;
    private int latestLteRSSI = -1;

    //Last and second-to-last cells visited
    private Cell currentCell = null;
    private Cell previousCell = null;

    private int currentlyDisplaying;    //Type of heatmap currently displayed (local copy of the select_display preference)
    private long cooldownTime;          //Local copy of the cooldown preference

    //Keeps track of the polygons that have been drawn on the map
    private List<Polygon> loadedPolygons;

    /*
    Keeps track of whether the app is in background or not to determine if notifications
    must be launched or not. Is set to true by MainActivity's onStop and to false by
    MainActivity's onStart.
     */
    private boolean notficationsAllowed = false;


    //Listeners and Receivers

    private BroadcastReceiver wifiResultsListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            latestWifiRSSI = computeWifiStrength(wifiManager.getScanResults());

            if (latestWifiRSSI >= EXCELLENT_RSSI) {
                //refer to notifyExcellentWifi
                notifyExcellentWifi();
            }

            if (currentCell == null) {return;} //ignore stray updates before location is available.

            Heat wifiHeat = new Heat(currentCell, Heat.TYPE_WIFI, latestWifiRSSI);
            pushNewMeasurement(wifiHeat);
        }
    };

    private PhoneStateListener signalStrengthListener = new PhoneStateListener() {
        //Coarse location, required by getAllCellInfo, is included in startListeningCellular
        @SuppressLint("MissingPermission")
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            //compute the new UMTS and LTE rssi
            latestUmtsRSSI = computeUmtsStrength(telephonyManager.getAllCellInfo());
            latestLteRSSI = computeLteStrength(signalStrength);

            if(currentCell == null) {return;} //ditto

            Heat umtsHeat = new Heat(currentCell, Heat.TYPE_UMTS, latestUmtsRSSI);
            Heat lteHeat = new Heat(currentCell, Heat.TYPE_LTE, latestLteRSSI);

            //refer to decideUmtsAndPush
            decideUmtsLteAndPush(umtsHeat,lteHeat);
        }
    };

    /*
    polygonClickListener handles the display of the "Cell info" dialog. It gets the data
    associated with the clicked polygon and builds an alert dialog out of it.
     */
    private GoogleMap.OnPolygonClickListener polygonListener = new GoogleMap.OnPolygonClickListener() {
        @Override
        public void onPolygonClick(final Polygon polygon) {
            //Try and get the associated heat
            Heat selectedHeat;
            try {
                selectedHeat = (Heat) polygon.getTag();
                if(selectedHeat == null) {return;} //no tag
            } catch (ClassCastException e) {
                //invalid tag
                return;
            }
            //tag is valid: proceed

            polygon.setStrokeWidth(STROKE_SELECTED); //highlight selected polygon

            //Build the text to display line by line
            CharSequence title = activity.getText(R.string.cell_info);
            CharSequence firstLine =
                    activity.getText(R.string.cell_coord)+": ("+selectedHeat.getCellX()+", "+selectedHeat.getCellY()+")";
            CharSequence type;
            switch(selectedHeat.getMeasurementType()) {
                case Heat.TYPE_WIFI: type = activity.getText(R.string.wifi); break;
                case Heat.TYPE_UMTS: type = activity.getText(R.string.umts); break;
                case Heat.TYPE_LTE: type = activity.getText(R.string.lte); break;
                default: return;
            }
            CharSequence secondLine =
                    activity.getText(R.string.meas_type)+": "+type;
            double percentage = makePrecentage(selectedHeat.getMeasurementRSSI(),MAX_RSSI);
            CharSequence thirdLine =
                    activity.getText(R.string.meas_strength)+": "+selectedHeat.getMeasurementRSSI()+" ("+percentage+"%)";
            CharSequence fourthLine =
                    activity.getText(R.string.num_meas)+": "+selectedHeat.getWeight();
            CharSequence fifthLine =
                    activity.getText(R.string.last_meas)+": "+makeTimeAgoDesc(selectedHeat.getTimestamp());

            //build dialog
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            dialogBuilder.setTitle(activity.getText(R.string.cell_info));
            dialogBuilder.setMessage(firstLine+"\n"+
                    secondLine+"\n"+
                    thirdLine+"\n"+
                    fourthLine+"\n"+
                    fifthLine+"\n"
            );
            //Whenever the user closes the dialog OR taps outside of it, the dialog is closed and the polygon un-highlighted
            dialogBuilder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    polygon.setStrokeWidth(STROKE_UNSELECTED);
                }
            });
            dialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    polygon.setStrokeWidth(STROKE_UNSELECTED);
                }
            });

            dialogBuilder.create().show();

        }
    };

    //Tasks

    /*
    UpdateCellTask is the most crucial task handled by the heatManager.
    When a new measurement is received, the following steps are carried out:

    - The time of the most recent measurement at the location is retrieved. If not enough
    time has elapsed for a new measurement, the task ends (returns null).
    - The new measurement is inserted into the database.
    - If the measurement concerned a technology that is not being displayed, the task ends
    (returns null).
    - Otherwise, the updated database data for the location is retrieved and passed to
    onPostExecute to be displayed.
    - If a polygon already exists that displays the old measurements, delete it.
    - Draw the updated polygon, associating it to the new heat.
    */
    private class UpdateCellTask extends AsyncTask<Heat, Void, Heat> {

        @Override
        protected Heat doInBackground(Heat... heats) {
            Heat heat = heats[0];

            long timeOfMostRecentMeasurement =
                    heatDatabase.getTimeOfLastMeasurementAtCellCoordinates(heat.getMeasurementType(),heat.getCellX(),heat.getCellY());
            if(heat.getTimestamp()-timeOfMostRecentMeasurement < cooldownTime) {
                //Cannot save new measurement
                return null;
            }

            heatDatabase.insert(heat);

            if(heat.getMeasurementType() != currentlyDisplaying) {
                //New heat is NOT of the currently displayed type, no need to proceed to display
                return null;
            }

            //get the new & updated average for the cell
            return heatDatabase.getAveragedHeatAtCellCoordinates(heat.getMeasurementType(), heat.getCellX(), heat.getCellY());
        }

        @Override
        protected void onPostExecute(Heat heat) {
            if(heat == null) {
                //cell was not updated or is not on display: stop.
                return;
            }

            //cell WAS updated and needs to be displayed
            Polygon oldPolygon = getCoincidingLoadedPolygon(heat);
            if (oldPolygon != null) {
                //erase old polygon before drawing
                oldPolygon.remove();
                loadedPolygons.remove(oldPolygon);
            }

            //display new polygon
            makePolygonFromHeat(heat);
        }
    }

    /*
    DrawScreenTask performs an update of the whole screen. Given the LatLngBounds of the screen,
    it computes which cells to show, retrieves the heats and draws them.
     */
    private class DrawScreenTask extends AsyncTask<LatLngBounds, Void, List<Heat>> {

        @Override
        protected List<Heat> doInBackground(LatLngBounds... latLngBounds) {
            LatLngBounds bounds = latLngBounds[0];

            //The bounds correspond to these cells
            Cell farRight = new Cell(bounds.northeast);
            Cell nearLeft = new Cell(bounds.southwest);

            int minX = nearLeft.getCellX()-1;
            int maxX = farRight.getCellX()+1;
            int minY = nearLeft.getCellY()-1;
            int maxY = farRight.getCellY()+1;

            //Get all averaged heats in the area
            return heatDatabase.getAveragedHeatsInArea(currentlyDisplaying, minX, maxX, minY, maxY);
        }

        @Override
        protected void onPostExecute(List<Heat> screen) {

            //Clear screen and unload old polygons
            for(Polygon p: loadedPolygons) {
                p.remove();
            }
            loadedPolygons.removeAll(loadedPolygons);

            for(Heat averagedHeat : screen) {
                //load and draw the new polygons
                makePolygonFromHeat(averagedHeat);
            }
        }
    }

    /*
    ResetDatabaseTask simply instructs the HeatDatabase to clear all tables, in background.
    Once that is done, onPostExecute removes all the polygons from the map and unloads them
    (removes them from loadedPolygons)
     */
    private class ResetDatabaseTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            HeatDatabase.getDatabase(activity).clearAllTables();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Snackbar.make(activity.findViewById(R.id.coord_container),R.string.database_reset_done_snack,Snackbar.LENGTH_SHORT).show();
            for(Polygon polygon : loadedPolygons) {
                polygon.remove();
            }
            loadedPolygons.removeAll(loadedPolygons);
        }
    }


    //private constructor (HeatManager is a singleton)
    private HeatManager() {
        loadedPolygons = new LinkedList<Polygon>();
    }

    //Public method, used to obtain the manager
    public static HeatManager getManager() {
        if (INSTANCE == null) {
            synchronized (HeatManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new HeatManager();
                }
            }
        }
        return INSTANCE;
    }

    /*
    Attaches the manager to an activity and a map. Used in MainActivity's onCreate.
    HeatManager uses activity to get the database, the starting preferences, the telephony
    and wifi managers, and to register its notification channel. HeatManager's direct
    access to map lets it handle drawing and other functions on its own.
     */
    public void attach(AppCompatActivity activity, GoogleMap map) {
        this.map = map;
        this.activity = activity;

        //get database
        heatDatabase = HeatDatabase.getDatabase(activity).heatDao();

        //get starting display and cooldown preferences
        SharedPreferences startingPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        currentlyDisplaying = Integer.parseInt(startingPreferences.getString("select_display","9"));
        switch (startingPreferences.getString("cooldown","10m")) {
            //init cooldownTime with the appropriate number of seconds
            case "none" : cooldownTime = 0; break;
            case "10m" : cooldownTime = 10*ONE_MINUTE_WORTH_OF_SECONDS; break;
            case "1h" : cooldownTime = ONE_HOUR_WORTH_OF_SECONDS; break;
            case "1d" : cooldownTime = ONE_DAY_WORTH_OF_SECONDS; break;
        }

        //get managers and attach respective listeners
        telephonyManager = (TelephonyManager) activity.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        startListeningCellular();
        wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        activity.registerReceiver(wifiResultsListener, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        //set map polygon click listener
        map.setOnPolygonClickListener(polygonListener);

        //last, but not least, register notification channel for versions >= 8.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = activity.getString(R.string.wifi_channel_name);
            String description = activity.getString(R.string.wifi_channel_desc);
            NotificationChannel channel = new NotificationChannel(WIFI_CHANNEL, name, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = activity.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /*
    First checks if the app has the appropriate permissions (coarse location), then sets the telephonyManager to
    listen for changes in signal strength and/or cell info.
     */
    public void startListeningCellular() {
        try {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        activity,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MainActivity.LOCATION_PERM_REQ);
                return;
            }
            telephonyManager.listen(signalStrengthListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS + PhoneStateListener.LISTEN_CELL_INFO);
        } catch (NullPointerException e) {
            /*
            catches case of first initialization of attached activity, in which startListeningCells could be
            invoked by onStart before the heatManager is attached to the view and to an actual telephonyManager.
            */
            return;
        }
    }

    //Stops the listening of changes in signal strength and cell info (called by MainActivity's onStop).
    public void stopListeningCellular() {
        telephonyManager.listen(signalStrengthListener, PhoneStateListener.LISTEN_NONE);
    }

    //Invoked by MainActivity on location change. Updates current cell, handles "best effort" measurements and requests wifi scans
    public void measureAtLocation(LatLng location) {
        previousCell = currentCell;
        currentCell = new Cell(location);

        if(previousCell != null && currentCell != previousCell) {
            /*
            We migrated to a new cell. We push "best effort" updates using the latest[type] variables, in case
            the cell we are leaving didn't receive any measurement changes.
             */
            Heat latestWifiHeat = new Heat(previousCell, Heat.TYPE_WIFI, latestWifiRSSI);
            pushNewMeasurement(latestWifiHeat);

            Heat latestUmtsHeat = new Heat(previousCell, Heat.TYPE_UMTS, latestUmtsRSSI);
            Heat latestLteHeat = new Heat(previousCell, Heat.TYPE_LTE, latestLteRSSI);
            decideUmtsLteAndPush(latestUmtsHeat, latestLteHeat);
        }

        wifiManager.startScan(); //require a scan for the new location
    }

    /*
    Invoked by wifiResultsListener when excellent (>250) wifi rssi is detected. Creates a notification with a pending
    intent for MainActivity and displays it.
     */
    private void notifyExcellentWifi() {
        if(!notficationsAllowed) {return;} //the app is running in foreground and we don't need to notify anything

        //create intent
        Intent responseIntent = new Intent(activity, MainActivity.class);
        responseIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, responseIntent, 0);

        //build notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(activity, WIFI_CHANNEL);
        notificationBuilder
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(activity.getText(R.string.notification_title))
                .setContentText(activity.getText(R.string.notification_desc))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        //Multiple notifications of this kind need not be displayed separately, so only 0 is used as ID
        NotificationManagerCompat.from(activity).notify(0, notificationBuilder.build());
    }


    //RSSI METHODS

    //Finds the three best networks in range and combines their signal strengths into a sigle WiFi rssi
    private int computeWifiStrength(List<ScanResult> results) {
        int level = 0;
        int networks = Math.min(MAX_WIFI_CONSIDERED, results.size());   //consider at most 3 networks
        for(int i = 0; i < networks; i++) {
            double networkLevel = WifiManager.calculateSignalLevel((results.get(i).level),256);
            //each network contributes to the final measurement with a PART of its signal strength
            level += (int) (networkLevel*WIFI_SCALE_FACTOR);
        }

        return Math.min(level,MAX_RSSI); //return rssi (255 if result is greater than 255)
    }

    //Finds the strongest Wcdma (UMTS) cell and returns the UMTS rssi
    private int computeUmtsStrength(List<CellInfo> cellInfos) {
        int asu = 0;

        for (CellInfo cellInfo : cellInfos) {
            CellInfoWcdma cellInfoWcdma;    //we are only interested in WCDMA cells

            try {
                cellInfoWcdma = (CellInfoWcdma) cellInfo;
            } catch (ClassCastException e) {
                //discard other cells
                continue;
            }

            int cellAsu = cellInfoWcdma.getCellSignalStrength().getAsuLevel();
            if (cellAsu == NeighboringCellInfo.UNKNOWN_RSSI) {
                //If cell has unknown signal, ignore it.
                continue;
            } else if (cellAsu > asu) {
                //Better cell found, replace measurement
                asu = cellAsu;
            }
        }

        //UMTS asu goes from 0 to 31. Map interval to 0-255 instead.
        return (int) (asu*(255.0/31.0));

    }

    //Uses reflection to compute the LTE-only rssi from a SignalStrength object
    private int computeLteStrength(SignalStrength signalStrength) {
        try {
            Method[] methods = android.telephony.SignalStrength.class.getMethods();
            for (Method mthd : methods) {
                if (mthd.getName().equals("getLteAsuLevel")) {
                    int asu = (int)mthd.invoke(signalStrength);
                    if(asu == NeighboringCellInfo.UNKNOWN_RSSI || asu == 255) {
                        //asu levels of 99 and 255 denote invalid/undetectable lte RSSI.
                        return 0;
                    } else {
                        //asu level goes from 0 to 97 included. Map this interval from 0 to 255 instead.
                        return (int)(asu*(255.0/97.0));
                    }
                }
            }
        } catch (IllegalAccessException
                | InvocationTargetException
                | SecurityException
                | IllegalArgumentException e1) {
            e1.printStackTrace();
        }

        //computation failed for some other reason
        return 0;
    }


    //DISPLAY METHODS

    //Computes the color to display based on type, intensity and weight of a measurement
    int computeGradientColor(int type, int rssi, int howmany) {
        /*
        Weight is decided as follows:
            1 measurement: 50 alpha (~20% transparency)
            2 measurement: 70 alpha
            3 measurement: 90 alpha
            4 measurement: 110 alpha
            5 measurement or more: 130 alpha (~50% transparency)
         */
        int weight = Math.min(howmany, 5)*20+25;

        /*
        A gradient uses the full range of two colors. The remaining color is fixed and used
        to give a more aesthetically pleasing result. The final range is therefore of 512 colors.
         */
        int primary = (int)(Math.min(rssi, HALF_RSSI)* RSSI_DOUBLING_FACTOR); //primary color is present for rssi > 0
        int secondary = (int)(Math.max(rssi-(HALF_RSSI+1),MIN_RSSI)* RSSI_DOUBLING_FACTOR); //secondary color is present for rssi > 127
        int base = 100; //just for the aesthetics of it

        //each technology assigns primary, secondary, base to different colors, to obtain three distinctive gradients
        if(type == Heat.TYPE_WIFI) {
            return Color.argb(weight,base,secondary,primary);
        } else if (type == Heat.TYPE_UMTS) {
            return Color.argb(weight,primary,base,secondary);
        } else if (type == Heat.TYPE_LTE) {
            return Color.argb(weight,secondary,primary,base);
        } else return 0;
    }

    //turns a Heat object into a polygon to be displayed on screen
    void makePolygonFromHeat(Heat heat) {
        //add a polygon to the map with appearance dictated by heat
        Polygon polygon = map.addPolygon(heat.getBounds()
                .fillColor(computeGradientColor(heat.getMeasurementType(),heat.getMeasurementRSSI(),heat.getWeight()))
                .strokeColor(Color.WHITE)
                .strokeWidth(0));
        polygon.setTag(heat);           //polygon references its heat for later use.
        loadedPolygons.add(polygon);    //keep track of polygon
        polygon.setClickable(true);
    }


    //TASK METHODS

    //Wrapper around UpdateCellTask. Ensures that one heat is pushed at a time
    private void pushNewMeasurement(Heat heat) {
        if(heat.getMeasurementRSSI() >= 0) {
            //avoid pushing heats coming from uninitialized values
            new UpdateCellTask().execute(heat);
        }
    }

    /*
    UMTS and LTE cannot be measured at the same time. Therefore, this method determines
    which one is actually being picked up (rssi > 0) and pushes a new measurement for it.
    */
    private void decideUmtsLteAndPush(Heat umtsHeat, Heat lteHeat) {
        if(umtsHeat.getMeasurementRSSI() <= 0) {
            if(lteHeat.getMeasurementRSSI() <= 0) {
                    /*
                    In the remarkable case in which none is being picked up, push both measurements
                    as equal to 0, as in this case there REALLY is no signal at all.
                     */
                pushNewMeasurement(umtsHeat);
                pushNewMeasurement(lteHeat);
            } else {
                //LTE is being picked up
                pushNewMeasurement(lteHeat);
            }
        } else {
            //UMTS is being picked up
            pushNewMeasurement(umtsHeat);
        }
    }

    /*Wrapper around DrawScreenTask. If the map is so zoomed out that the cells would be invisible,
    don't update the screen (note that already-drawn cells are kept where they are and do not disappear)*/
    public void updateScreen() {
        if (map.getCameraPosition().zoom >= MIN_DRAWABLE_ZOOM) {
            new DrawScreenTask().execute(map.getProjection().getVisibleRegion().latLngBounds);
        }
    }

    //Wrapper around ResetDatabaseTask
    public void resetDatabase() {
        new ResetDatabaseTask().execute();
    }


    //SETTINGS METHODS

    //Used by SettingsFragment fragment to notify changes in display preference
    public void setDisplay(int type) {
        currentlyDisplaying = type;
    }

    //Used by SettingsFragment fragment to notify changes in cooldown preference
    public void setCooldownTime(long seconds) {
        cooldownTime = seconds;
    }


    //UTILITY METHODS

    //Checks wheter a polygon already exists that displays heat and returns it in case
    Polygon getCoincidingLoadedPolygon(Heat heat) {
        for(Polygon polygon : loadedPolygons) {
            Heat polygonHeat = (Heat) polygon.getTag();
            if (polygonHeat.getCellX() == heat.getCellX() &&
                    polygonHeat.getCellY() == heat.getCellY() &&
                    polygonHeat.getMeasurementType() == heat.getMeasurementType()) {
                //polygon displays the same location and type of heat.
                return polygon;
            }
        }
        return null;
    }

    //Turns a timestamp into a natural description of how many days/hours/etc... have passed since
    CharSequence makeTimeAgoDesc(long timestamp) {
        long currentTime = System.currentTimeMillis()/1000;
        long distance = currentTime-timestamp;
        CharSequence desc;
        if (distance > ONE_DAY_WORTH_OF_SECONDS) {
            return (distance/ONE_DAY_WORTH_OF_SECONDS)+" "+activity.getText(R.string.days_ago);
        } else if (distance > ONE_HOUR_WORTH_OF_SECONDS) {
            return (distance/ONE_HOUR_WORTH_OF_SECONDS)+" "+activity.getText(R.string.hours_ago);
        } else if (distance > ONE_MINUTE_WORTH_OF_SECONDS) {
            return (distance/ONE_MINUTE_WORTH_OF_SECONDS)+" "+activity.getText(R.string.minutes_ago);
        } else if (distance > 10){
            return distance+" "+activity.getText(R.string.seconds_ago);
        } else if (distance >= 0){
            return activity.getText(R.string.moments_ago);
        } else
            return activity.getText(R.string.future);
    }

    //Turns num/den into a percentage with at most two significant digits
    private double makePrecentage(int num, int den) {
        //I am not proud of this piece of code
        return (double)((int)(10000.0*num/den))/100;
    }

    //Sets notificationsAllowed.
    public void setNotficationsAllowed(boolean b) {
        notficationsAllowed = b;
    }
}
