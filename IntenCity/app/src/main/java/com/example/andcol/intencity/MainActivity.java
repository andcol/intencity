package com.example.andcol.intencity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity
        implements HelpFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener{

    //Codes for permission requests
    public static final int LOCATION_PERM_REQ = 11;
    public static final int STORAGE_PERM_REQ = 44;

    //Constants used to restore/reinit camera position
    private static final String LAST_CAMERA_KEY = "LastCamera";
    private final static int GOTO_ZOOM = 15;

    //Interface and Navigation
    private Toolbar toolbar;
    private DrawerLayout mDrawer;
    private NavigationView nvDrawer;

    //Fragments
    private FragmentManager fragmentManager;
    private SupportMapFragment mapFragment = new SupportMapFragment();
    private SettingsFragment settingsFragment = new SettingsFragment();
    private HelpFragment helpFragment = new HelpFragment();

    //Location variables
    private FusedLocationProviderClient locationClient;
    private LocationRequest locationRequest = new LocationRequest();
    private GoogleMap map;
    private LatLng currentLocation;
    private CameraPosition currentCamera;
    boolean justStartedTracking = true;

    //Other Managers
    private HeatManager heatManager;    //the associated heat manager


    //CALLBACKS

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult result) {
            //refer to changeLocation
            changeLocation(result.getLastLocation());
        }
    };
    private GoogleMap.SnapshotReadyCallback snapshotReadyCallback = new GoogleMap.SnapshotReadyCallback() {
        @Override
        public void onSnapshotReady(Bitmap bitmap) {
            //Saving snapshot requires storage permission
            if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },STORAGE_PERM_REQ);
                return;
            }
            //If permission is granted, proceed (refer to shareSnapshot).
            shareSnapshot(bitmap);
        }
    };


    //LISTENERS

    //Handles responses to navigation
    private NavigationView.OnNavigationItemSelectedListener navClickListener =
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    item.setChecked(true);
                    mDrawer.closeDrawers();

                    switch(item.getItemId()) {
                        case R.id.map: {
                            //switch to map fragment
                            fragmentManager.beginTransaction().replace(R.id.coord_container, mapFragment).addToBackStack("MapFragment").commit();
                            return true;
                        }
                        case R.id.share: {
                            //produce a snapshot (refer to shareSnapshot)
                            map.snapshot(snapshotReadyCallback);
                            return true;
                        }
                        case R.id.settings: {
                            //switch to settings fragment
                            fragmentManager.beginTransaction().replace(R.id.coord_container,settingsFragment).addToBackStack("SettingsManager").commit();
                            return true;
                        }
                        case R.id.helpscreen: {
                            //switch to help fragment
                            fragmentManager.beginTransaction().replace(R.id.coord_container, helpFragment).addToBackStack("HelpFragment").commit();
                            return true;
                        }
                    }
                    return false;
                }
            };

    private GoogleMap.OnCameraIdleListener cameraIdleListener = new GoogleMap.OnCameraIdleListener() {
        @Override
        public void onCameraIdle() {
            currentCamera = map.getCameraPosition(); //save camera position every time the camera stops
            heatManager.updateScreen(); //ask the heatManager to draw the screen at the new position
        }
    };


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the variables
        toolbar = findViewById(R.id.toolbar);
        mDrawer = findViewById(R.id.drawer_layout);
        nvDrawer = findViewById(R.id.nvView);

        //get the fragment manager and the FusedLocationProvider
        fragmentManager = getSupportFragmentManager();
        locationClient = LocationServices.getFusedLocationProviderClient(this);

        /*
        Set-up the request for location updates. Updates are received:
            -with high accuracy
            -at most once every 5 seconds
            -if the displacement is at least 15 meters (adequate considering cell size)
         */
        locationRequest.setInterval(5000).setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setSmallestDisplacement(15);

        //replace the actionbar and set menu button
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        if(findViewById(R.id.tablet_linear_layout)== null) {
            //We are not in tablet mode, so we need the "sandwich" button
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }

        //set navClickListener to respond to navigation events
        nvDrawer.setNavigationItemSelectedListener(navClickListener);

        //Get the heatManager
        heatManager = HeatManager.getManager();

        //set the map fragment and get the map
        fragmentManager.beginTransaction().add(R.id.coord_container,mapFragment).commit();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                //Once the map is ready, we can...

                //get and style the map
                map = googleMap;
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(MainActivity.this, R.raw.map_style));

                //restore position if necessary
                if (savedInstanceState != null) {
                    //Activity has a bundle (screen orientation change case), restore location
                    CameraPosition oldPosition = savedInstanceState.getParcelable(LAST_CAMERA_KEY);
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(oldPosition));
                    justStartedTracking = false; //as we have restored the position already
                }
                map.setOnCameraIdleListener(cameraIdleListener);

                heatManager.attach(MainActivity.this, map); //attach the manager to activity and to map
                startTracking(); //register for location updates
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        //stop location and cellular updates and enable notifications from the heatManager
        stopTracking();
        heatManager.stopListeningCellular();
        heatManager.setNotficationsAllowed(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        //register location and cellular updates again
        startTracking();
        heatManager.startListeningCellular();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //notifications are not allowed from heatManager if MainActivity is running in foreground
        heatManager.setNotficationsAllowed(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putParcelable(LAST_CAMERA_KEY, map.getCameraPosition());  //remember last camera position
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERM_REQ: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Success, restart startTracking
                    map.setMyLocationEnabled(true);         //we can now display current position
                    heatManager.startListeningCellular();   //we can now listen for cellular data
                    startTracking();                        //we can now start receiving location updates
                } else if (grantResults.length > 0) {
                    //Failure in obtaining permissions
                    snackAnnounce(getText(R.string.location_unauth_err_snack),Snackbar.LENGTH_INDEFINITE);
                }
                return;
            }
            case STORAGE_PERM_REQ : {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    map.snapshot(snapshotReadyCallback);    //we can now take a snapshot
                } else if (grantResults.length > 0) {
                    snackAnnounce(getText(R.string.snapshot_unauth_err_snack),Snackbar.LENGTH_LONG);
                }
                return;
            }
            default: return;
        }
    }

    private void startTracking() {

        //if permissions have not been granted, request them and abort startTracking
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE},
                    LOCATION_PERM_REQ);
            return;
        } //else proceed

        try {
            map.setMyLocationEnabled(true);
        } catch (NullPointerException e) {
            /*
            catches case of first initialization of activity, in which startTracking could be
            invoked by onStart before the map has been asynchronously received. (This try block could probably be removed altogether)
            */
            return;
        }
        locationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private void stopTracking () {
        locationClient.removeLocationUpdates(locationCallback);
    }

    /*
    changeLocation is invoked by locationCallback. It handles errors in location retrieval, the update of location and camera
    and triggers an update in heatManager.
     */
    private void changeLocation(Location location) {

        if(location == null) {
            snackAnnounce(getText(R.string.location_failed_err_snack),Snackbar.LENGTH_LONG);
            return;
        }
        currentLocation = new LatLng(location.getLatitude(),location.getLongitude()); //update current location
        if(justStartedTracking) {
            //If the activity just started, jump to the first received location
            CameraPosition.Builder builder = new CameraPosition.Builder()
                    .target(currentLocation)
                    .zoom(GOTO_ZOOM);
            map.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
            justStartedTracking = false;
        }
        heatManager.measureAtLocation(currentLocation); //trigger an update in heatMeasurement

    }

    public void shareSnapshot(Bitmap bitmap) {
        //Prepare output
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        //Prepare file
        File tmp = new File(MainActivity.this.getExternalFilesDir(null),"tmp_img");
        if(!tmp.exists()) {
            //make directory if it does not exist
            tmp.mkdirs();
        }
        File f = new File(tmp, "tmp_snapshot.jpg");
        try {
            f.createNewFile();
            FileOutputStream outputFile = new FileOutputStream(f);
            outputFile.write(stream.toByteArray());
            outputFile.close();
        } catch (IOException e) {
            snackAnnounce(getText(R.string.share_failed_err),Snackbar.LENGTH_LONG);
            e.printStackTrace();
            return;
        }
        Uri contentUri = FileProvider.getUriForFile(MainActivity.this,"com.example.andcol.fileprovider",f);


        //declare intent
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/jpg");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, contentUri);
        //launch
        startActivity(Intent.createChooser(intent,getText(R.string.share_title)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //required by fragments
    @Override
    public void onFragmentInteraction(Uri uri) {
        return;
    }

    //util: create a snackbar in the appropriate coordinator container.
    private void snackAnnounce(CharSequence message, int length) {
        Snackbar.make(findViewById(R.id.coord_container), message, length).show();
    }
}


